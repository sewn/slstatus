# slstatus version
VERSION = 1.0

# customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

HOSTNAME != hostname

# flags
CPPFLAGS = -D_DEFAULT_SOURCE -DALSA -DHOSTNAME_${hostname} -DVERSION=\"${VERSION}\"
CFLAGS   = -std=c99 -pedantic -Wall -Wextra -Wno-unused-parameter -O0
LDFLAGS  = -s
# OpenBSD: add -lsndio
# FreeBSD: add -lkvm -lsndio
LDLIBS   = -lasound

# compiler and linker
CC = cc
